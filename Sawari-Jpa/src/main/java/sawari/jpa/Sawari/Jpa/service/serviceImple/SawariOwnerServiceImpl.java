package sawari.jpa.Sawari.Jpa.service.serviceImple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sawari.jpa.Sawari.Jpa.dto.LocationDTO;
import sawari.jpa.Sawari.Jpa.dto.SawariOwnerDTO;
import sawari.jpa.Sawari.Jpa.entity.SawariOwner;
import sawari.jpa.Sawari.Jpa.repository.SawariownerReporsitory;
import sawari.jpa.Sawari.Jpa.service.SawariOwnerService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class SawariOwnerServiceImpl implements SawariOwnerService {
    @Autowired
    private SawariownerReporsitory sawariownerReporsitory;
    @Override
    public SawariOwner create(SawariOwner sawariOwner) {
        LocalDateTime date = LocalDateTime.now();
        sawariOwner.setCreated_date(date.toString());
        sawariOwner.setStatus("active");
        sawariOwner.setUpdated_at(date.toString());
        return sawariownerReporsitory.save(sawariOwner);
    }

    @Override
    public SawariOwnerDTO getByid(Long id) {
        Optional<SawariOwner> optionalSawariOwner = sawariownerReporsitory.findBySawariId(id);
        SawariOwnerDTO sawariOwnerDTO = new SawariOwnerDTO();
        if(!optionalSawariOwner.isEmpty()){

            SawariOwner sawariOwner=optionalSawariOwner.get();
            sawariOwnerDTO = this.mapResult(sawariOwner);

        }

        return sawariOwnerDTO;
    }

    @Override
    public List<SawariOwnerDTO> getAll() {

      //  return sawariownerReporsitory.findAll();
        List<SawariOwner> sawariOwners = sawariownerReporsitory.findAll();
        List<SawariOwnerDTO> sawariOwnerDTOS = new ArrayList<>();
        if(sawariOwners!=null){
            for(SawariOwner sawariOwner:sawariOwners){

                sawariOwnerDTOS.add(this.mapResult(sawariOwner));
            }
        }
        return sawariOwnerDTOS;
    }

    @Override
    public SawariOwnerDTO update(SawariOwner sawariOwner) {
        LocalDateTime dateTime = LocalDateTime.now();
        SawariOwner existingOwner = sawariownerReporsitory.findById(sawariOwner.getId()).get();
        existingOwner.setName(sawariOwner.getName());
        existingOwner.setPhone(sawariOwner.getPhone());
        existingOwner.setEmail(sawariOwner.getEmail());
        existingOwner.setAddress(sawariOwner.getAddress());
        existingOwner.setCitizenship_no(sawariOwner.getCitizenship_no());
        existingOwner.setCitizenship_photo(sawariOwner.getCitizenship_photo());
        existingOwner.setLocation(sawariOwner.getLocation());
        existingOwner.setUpdated_at(dateTime.toString());
        existingOwner.setPhoto(sawariOwner.getPhoto());
        SawariOwner updatedUser = sawariownerReporsitory.save(existingOwner);
        return this.mapResult(updatedUser);
    }

    @Override
    public void delete(Long id) {
         sawariownerReporsitory.deleteById(id);
    }
    private SawariOwnerDTO mapResult(SawariOwner sawariOwner){
        SawariOwnerDTO sawariOwnerDTO = new SawariOwnerDTO();
        sawariOwnerDTO.setName(sawariOwner.getName());
        sawariOwnerDTO.setAddress(sawariOwner.getAddress());
        sawariOwnerDTO.setPhone(sawariOwner.getPhone());
        sawariOwnerDTO.setEmail(sawariOwner.getEmail());
        sawariOwnerDTO.setCitizenship_no(sawariOwner.getCitizenship_no());
        sawariOwnerDTO.setCitizenship_photo(sawariOwner.getCitizenship_photo());
        sawariOwnerDTO.setPhoto(sawariOwner.getPhoto());
        sawariOwnerDTO.setStatus(sawariOwner.getStatus());
        if(sawariOwner.getLocation()!=null){
            LocationDTO locationDto = new LocationDTO();
            locationDto.setLocation(sawariOwner.getLocation().getLocation());
            sawariOwnerDTO.setLocation(locationDto);
        }
        return sawariOwnerDTO;
    }
}
