package sawari.jpa.Sawari.Jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sawari.jpa.Sawari.Jpa.dto.SawariOwnerDTO;
import sawari.jpa.Sawari.Jpa.dto.SawariVehiclesDTO;
import sawari.jpa.Sawari.Jpa.entity.SawariVehicles;
import sawari.jpa.Sawari.Jpa.service.SawariVehicleService;

import java.util.List;

@RestController
@RequestMapping("vehicles")
public class SawariVehiclesController {
    @Autowired
    private SawariVehicleService sawariVehicleService;
    @GetMapping("all")
    public ResponseEntity<List<SawariVehiclesDTO>> getAll(){
        List<SawariVehiclesDTO> sawariVehiclesDTOS = sawariVehicleService.getAll();
        return new ResponseEntity<>(sawariVehiclesDTOS, HttpStatus.OK);
    }
    @GetMapping("detail/{id}")
    public ResponseEntity<SawariVehiclesDTO> getById(@PathVariable("id") Long id){
        SawariVehiclesDTO sawariVehiclesDTO = sawariVehicleService.getById(id);
        return new ResponseEntity<>(sawariVehiclesDTO,HttpStatus.OK);
    }
    @GetMapping("booked")
    public ResponseEntity<List<SawariVehiclesDTO>> getBooked(){
        List<SawariVehiclesDTO> sawariVehiclesDTOS = sawariVehicleService.getBookedVehicles();
        return new ResponseEntity<>(sawariVehiclesDTOS,HttpStatus.OK);
    }
    @PostMapping("create")
    public ResponseEntity<SawariVehiclesDTO> create(@RequestBody SawariVehicles sawariVehicles){
        SawariVehiclesDTO sawariVehiclesDTO = sawariVehicleService.create(sawariVehicles);
        return new ResponseEntity<>( sawariVehiclesDTO,HttpStatus.CREATED);
    }
}
