package sawari.jpa.Sawari.Jpa.utility;

import net.bytebuddy.utility.RandomString;
import org.springframework.web.multipart.MultipartFile;
import sawari.jpa.Sawari.Jpa.controller.UploadFileController;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Scanner;

public class FileUploadUtility {
    public static String fileUpload(String fileName, MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get("files");
        if(!Files.exists(uploadPath)){
            Files.createDirectory(uploadPath);
        }
        String fileCode = RandomString.make(8);
        try{
            InputStream inputStream = multipartFile.getInputStream();
        Path filePath = uploadPath.resolve(fileCode+"-"+fileName);
        Files.copy(inputStream,filePath, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException ioe){
            throw new IOException("Could not save file"+fileName,ioe);
        }
        return uploadPath +"/"+ fileCode+fileName;
    }
    public static String readTextFile(String filePath) throws  IOException{
        //return UploadFileController.class.getResource("").getPath();
        File file = new File("/home/hiramani/Downloads/Sawari Jpa/"+filePath);
        Scanner scanner = new Scanner(file);
        StringBuilder content = new StringBuilder();
        while (scanner.hasNextLine()){
            content=content.append(scanner.nextLine()+"\n");
        }
        return content.toString();
    }

//    public static void main(String[] args) throws IOException{
//        String file = "/home/hiramani/Downloads/Sawari Jpa/files/abc.txt";
//        Path path = Paths.get(file);
//        List<String> lines = Files.readAllLines(path);
//        System.out.println(lines);
//    }
}
