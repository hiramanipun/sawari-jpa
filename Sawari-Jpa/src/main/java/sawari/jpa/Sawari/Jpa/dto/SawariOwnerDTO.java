package sawari.jpa.Sawari.Jpa.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import sawari.jpa.Sawari.Jpa.entity.Location;

@Getter
@Setter
@ToString
public class SawariOwnerDTO {
    private String name;
    private String phone;
    private String email;
    private String address;
    private String citizenship_no;
    private String citizenship_photo;
    private String status;
    private String Photo;
    private LocationDTO location;
}
