package sawari.jpa.Sawari.Jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sawari.jpa.Sawari.Jpa.entity.SawariBookedDetails;
import sawari.jpa.Sawari.Jpa.repository.BookedDetailsRepository;
import sawari.jpa.Sawari.Jpa.service.SawariBookedDetailsService;

import java.util.List;

@RestController
@RequestMapping("booked")
public class SawariBookedDetailsController {
    @Autowired
    private SawariBookedDetailsService sawariBookedDetailsService;
    @Autowired
    private BookedDetailsRepository bookedDetailsRepository;
    public SawariBookedDetailsController(BookedDetailsRepository bookedDetailsRepository){
        this.bookedDetailsRepository = bookedDetailsRepository;
    }
    @GetMapping("all")
    public ResponseEntity<List<SawariBookedDetails>> getAll(){
       List<SawariBookedDetails> all = sawariBookedDetailsService.getAll();
        return new ResponseEntity<>(all, HttpStatus.OK);
    }
    @GetMapping("details/{id}")
    public ResponseEntity<SawariBookedDetails> getById(@PathVariable("id") Long id){
        SawariBookedDetails sawariBookedDetails = sawariBookedDetailsService.getById(id);
        return  new ResponseEntity<>(sawariBookedDetails,HttpStatus.OK);
    }
    @GetMapping("booked")
    public ResponseEntity<List<SawariBookedDetails>> getBooked(){
     List<SawariBookedDetails> bookedVehicles = sawariBookedDetailsService.getBookedDetails();
     return new ResponseEntity<>(bookedVehicles,HttpStatus.OK);
    }

    @GetMapping("pending")
    public ResponseEntity<List<SawariBookedDetails>> getPending(){
        List<SawariBookedDetails> pendingBooks = sawariBookedDetailsService.getPendingDetails();
        return new ResponseEntity<>(pendingBooks,HttpStatus.OK);
    }
}
