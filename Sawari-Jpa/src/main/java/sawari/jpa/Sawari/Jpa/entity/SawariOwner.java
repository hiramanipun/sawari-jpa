package sawari.jpa.Sawari.Jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sawari_owner")
public class SawariOwner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String phone;
    @Column(nullable = false,unique = true)
    private String email;
    @Column(nullable = false)
    private String address;
    @Column(nullable = true)
    private  String citizenship_no;
    @Column(nullable = true)
    private String citizenship_photo;
    @Column(nullable = false)
    private String status;
    @Column(nullable = false)
    private String created_date;
    @Column(nullable = true)
    private Long fk_user_id;
//    @Column(name = "fk_location_id",nullable = true)
//    private Long FkLocationId;
    @Column(nullable = true)
    private String updated_at;
    @Column(nullable = true)
    private String photo;
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "fk_location_id",referencedColumnName = "id")
    private Location location;
    @OneToMany(mappedBy = "sawariOwner",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SawariVehicles> sawariVehicles;


}
