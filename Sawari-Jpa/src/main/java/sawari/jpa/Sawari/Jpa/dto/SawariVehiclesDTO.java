package sawari.jpa.Sawari.Jpa.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SawariVehiclesDTO {
    private LocationDTO locationDTO;
    private VehicleModelsDTO vehicleModelsDTO;
    private VehicleTypeDTO vehicleTypeDTO;
    private Long total_seats;
    private String color;
    private String AcNoAc;
    private String gadiNo;
    private String gadikoWiwaran;
    private String image;
    private String createdDate;
}
