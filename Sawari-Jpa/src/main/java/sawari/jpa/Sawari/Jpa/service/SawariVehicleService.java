package sawari.jpa.Sawari.Jpa.service;

import sawari.jpa.Sawari.Jpa.dto.SawariOwnerDTO;
import sawari.jpa.Sawari.Jpa.dto.SawariVehiclesDTO;
import sawari.jpa.Sawari.Jpa.entity.SawariVehicles;

import java.util.List;

public interface SawariVehicleService {
    public SawariVehiclesDTO create(SawariVehicles sawariVehicles);
    public SawariVehiclesDTO update(SawariVehicles sawariVehicles);
    public SawariVehiclesDTO getById(Long id);
    public List<SawariVehiclesDTO> getAll();
    public List<SawariVehiclesDTO> getBookedVehicles();
    public int delete(Long id);
}
