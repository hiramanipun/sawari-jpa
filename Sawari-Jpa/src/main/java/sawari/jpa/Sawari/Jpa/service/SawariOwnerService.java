package sawari.jpa.Sawari.Jpa.service;

import sawari.jpa.Sawari.Jpa.dto.SawariOwnerDTO;
import sawari.jpa.Sawari.Jpa.entity.SawariOwner;

import java.util.List;

public interface SawariOwnerService {
    SawariOwner create(SawariOwner sawariOwner);
    SawariOwnerDTO getByid(Long id);
    List<SawariOwnerDTO> getAll();
    SawariOwnerDTO update(SawariOwner sawariOwner);
    void delete(Long id);

 }
