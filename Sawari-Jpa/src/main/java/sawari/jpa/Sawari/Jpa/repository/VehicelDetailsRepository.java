package sawari.jpa.Sawari.Jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sawari.jpa.Sawari.Jpa.entity.SawariVehicles;

import java.util.List;
import java.util.Optional;

@Repository
public interface VehicelDetailsRepository extends JpaRepository<SawariVehicles,Long> {
    @Query("SELECT t FROM SawariVehicles t WHERE t.id=:id")
    Optional<SawariVehicles> getVehicleDetails(Long id);
    @Query("SELECT t FROM SawariVehicles t")
    List<SawariVehicles> getAll();
    @Query("SELECT t FROM SawariVehicles t WHERE t.StatusChar = 'available'")
    List<SawariVehicles> getBookedVehicles();
}
