package sawari.jpa.Sawari.Jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sawari.jpa.Sawari.Jpa.entity.Users;

public interface UsersRepository extends JpaRepository<Users,Long> {
}
