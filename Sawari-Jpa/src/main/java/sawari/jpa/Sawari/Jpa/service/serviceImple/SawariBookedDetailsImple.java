package sawari.jpa.Sawari.Jpa.service.serviceImple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sawari.jpa.Sawari.Jpa.entity.SawariBookedDetails;
import sawari.jpa.Sawari.Jpa.repository.BookedDetailsRepository;
import sawari.jpa.Sawari.Jpa.service.SawariBookedDetailsService;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class SawariBookedDetailsImple implements SawariBookedDetailsService {
@Autowired
    private BookedDetailsRepository bookedDetailsRepository;

    @Override
    public List<SawariBookedDetails> getAll() {

        return bookedDetailsRepository.findAll();
    }

    @Override
    public SawariBookedDetails getById(Long id) {
        Optional<SawariBookedDetails> optionalSawariBookedDetails = bookedDetailsRepository.findById(id);
        return optionalSawariBookedDetails.get();
    }

    @Override
    public List<SawariBookedDetails> getPendingDetails() {
       Optional<List<SawariBookedDetails>> pendingBooks = bookedDetailsRepository.getPendingBooks();
       return pendingBooks.get();
    }

    @Override
    public List<SawariBookedDetails> getBookedDetails() {
        Optional<List<SawariBookedDetails>> bookedVehicles = bookedDetailsRepository.getBookedVehicles();
        return  bookedVehicles.get();
    }
}
