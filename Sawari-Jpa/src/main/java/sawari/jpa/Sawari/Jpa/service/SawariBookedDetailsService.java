package sawari.jpa.Sawari.Jpa.service;

import sawari.jpa.Sawari.Jpa.entity.SawariBookedDetails;

import java.util.List;

public interface SawariBookedDetailsService {
    public List<SawariBookedDetails> getAll();
    public SawariBookedDetails getById(Long id);
    public List<SawariBookedDetails> getPendingDetails();
    public List<SawariBookedDetails> getBookedDetails();
}
