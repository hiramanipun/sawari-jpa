package sawari.jpa.Sawari.Jpa.controller;

import io.swagger.models.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sawari.jpa.Sawari.Jpa.utility.FileUploadUtility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("file")

public class UploadFileController {
    @PostMapping("upload")
    public String uploadFile(@RequestParam("file") MultipartFile multipartFile) throws IOException{
        String fileName = multipartFile.getOriginalFilename();
        String fileCode = FileUploadUtility.fileUpload(fileName,multipartFile);
        return fileCode;
    }
    @PostMapping("upload-multiples")
    public ResponseEntity<List<String>> uplaodFiles(@RequestParam("files") MultipartFile[] multipartFiles)throws IOException{
        List<String> fileNames = new ArrayList<>();
        for(MultipartFile multipartFile:multipartFiles){
            String fileName = multipartFile.getOriginalFilename();
            String filePath = FileUploadUtility.fileUpload(fileName,multipartFile);
            fileNames.add(filePath);
        }
        return new ResponseEntity<>(fileNames, HttpStatus.OK);

        }
        @PostMapping("read-text")
    public String readTextFile(@RequestParam("file") MultipartFile multipartFile)throws IOException{
        String fileName = multipartFile.getOriginalFilename();
        String filePath = FileUploadUtility.fileUpload(fileName,multipartFile);
        return FileUploadUtility.readTextFile(filePath);
        }

}
