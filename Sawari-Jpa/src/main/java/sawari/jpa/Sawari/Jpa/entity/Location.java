package sawari.jpa.Sawari.Jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sawari_location")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Location {
    @Id
    private Long id;
    @Column(nullable = false)
    private String location;
    @Column(nullable = false)
    private String created_date;
    @Column(nullable = false)
    private String status;
    @Column(nullable = false)
    private Long fk_user_id;

    @OneToMany(mappedBy = "location",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SawariOwner> sawariOwner;
    @OneToMany(mappedBy = "bookedLocation",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SawariBookedDetails> sawariBookedDetails;
    @OneToMany(mappedBy = "vehicleLocation",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SawariVehicles> sawariVehicles;

}
