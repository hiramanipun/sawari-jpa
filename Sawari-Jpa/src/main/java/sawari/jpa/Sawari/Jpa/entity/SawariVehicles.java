package sawari.jpa.Sawari.Jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sawari_vehicle_details")
public class SawariVehicles {
   public enum StatusChar{
    available,
       unavailable
    }
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name = "total_seats",nullable = false)
    private Long totalSeats;
    @Column(name = "color",nullable = false)
    private String color;
    @Column(name = "ac_noac",nullable = false)
    private Long acNoAc;
    @Column(name = "status_num",nullable = false)
    private int StatusNum;
    @Column(name = "status_char",nullable = false)
    private String StatusChar;
    @Column(name = "created_date",nullable = false)
    private String createdDate;
    @Column(name = "fk_user_id",nullable = false)
    private int fkUserId;
    @Column(name = "image",nullable = true)
    private String image;
    @Column(name = "gadi_no",nullable = false)
    private String gadiNo;
    @Column(name = "gadiko_wiwaran",nullable = true)
    private String gadikoWiwaran;

  //@JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_owner_id",referencedColumnName = "id")
    private SawariOwner sawariOwner;
  //@JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "fk_vehicle_model",referencedColumnName = "id")
    private VehicleModels vehicleModels;
  //@JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "fk_vehicle_type",referencedColumnName = "id")
    private VehicleType vehicleType;
  //@JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_location_id",referencedColumnName = "id")
    private Location vehicleLocation;

}
