package sawari.jpa.Sawari.Jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Users {
    @Id
    private Long id;
    @Column(name = "username",nullable = false)
    private String username;
    @Column(name = "password",nullable = false)
    private String password;
    @Column(name = "auth_key",nullable = false)
    private  String authKey;
    @Column(name = "name",nullable = false)
    private String name;
    @Column(name = "phone",nullable = false)
    private String phone;
    @Column(name = "email",nullable = true)
    private String email;
    @Column(name = "photo",nullable = true)
    private String photo;
    @Column(name = "fk_province_id",nullable = false)
    private Long fkProvinceId;
    @Column(name = "fk_district_id",nullable = false)
    private Long FkDistrictId;
    @Column(name = "fk_municipal_id",nullable = false)
    private Long fkMunicipal_id;
    @Column(name = "address",nullable = false)
    private String address;
    @Column(name = "others",nullable = true)
    private String other;
    @Column(name = "user_type",nullable = false)
    private Long userType;
    @Column(name = "created_at",nullable = false)
    private LocalDate createdAt;
    @Column(name = "updated_at",nullable = true)
    private LocalDate updatedAt;
    @Column(name = "status",nullable = false)
    private Long status;
    @Column(name = "fk_user_id",nullable = true)
    private Long fkUserId;
    @Column(name = "fk_service_place",nullable = true)
    private Long fkServicePlace;
    @Column(name = "access_token",nullable = false)
    private String accessToken;
    @Column(name = "firebase_token",nullable = true)
    private String firebaseToken;
    @Column(name = "sawari_owner_id",nullable = true)
    private Long sawariOwnerId;



}
