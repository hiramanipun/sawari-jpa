package sawari.jpa.Sawari.Jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "sawari_booked_details")
@Getter
@Setter
public class SawariBookedDetails {
    @Id
    private Long id;
    @Column(nullable = false)
    private Long fk_vehicle_id;
    @Column(nullable = false)
    private String booked_date;
    @Column(nullable = true)
    private String complete_date;
//    @Column(nullable = false)
//    private Long booking_from;
    @Column(nullable = false)
    private  String booking_to;
    @Column(nullable = false)
    private String booking_type;
    @Column(nullable = false)
    private String booking_days;
    @Column(nullable = true)
    private String  remarks;
    @Column(nullable = false)
    private Long fk_booked_by;
    @Column(nullable = false)
    private String fuel;
    @Column(nullable = false)
    private String booked_at;
    @Column(nullable = false)
    private Long fk_owner_id;
    @Column(nullable = false)
    private String status;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "booking_from",referencedColumnName = "id")
    private Location bookedLocation;
}
