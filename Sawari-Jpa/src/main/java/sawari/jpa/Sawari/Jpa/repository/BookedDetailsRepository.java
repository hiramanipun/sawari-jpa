package sawari.jpa.Sawari.Jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import sawari.jpa.Sawari.Jpa.entity.SawariBookedDetails;

import java.util.List;
import java.util.Optional;

public interface BookedDetailsRepository extends JpaRepository<SawariBookedDetails,Long> {
   @Query("SELECT t FROM SawariBookedDetails t WHERE t.status ='pending'")
    Optional<List<SawariBookedDetails>> getPendingBooks();
    @Query("SELECT t FROM SawariBookedDetails t WHERE t.status ='booked'")
    Optional<List<SawariBookedDetails>> getBookedVehicles();
}
