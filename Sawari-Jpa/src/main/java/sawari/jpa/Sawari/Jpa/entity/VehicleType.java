package sawari.jpa.Sawari.Jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sawari_vehicle_type")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VehicleType {
    @Id
    private Long id;
    @Column(name = "type",nullable = false)
    private String type;
    @Column(name = "created_date",nullable = false)
    private String createdDate;
    @Column(name = "remarks",nullable = true)
    private String remarks ;
    @Column(name = "status",nullable = false)
    private String status;
    @Column(name = "fk_user_id",nullable = false)
    private Long fkUserId;
    @OneToMany(mappedBy = "vehicleType",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<VehicleModels> vehicleModelsList;
    @OneToMany(mappedBy = "vehicleType",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SawariVehicles> sawariVehicles;

}
