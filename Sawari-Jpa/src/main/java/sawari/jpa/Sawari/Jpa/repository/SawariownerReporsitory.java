package sawari.jpa.Sawari.Jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sawari.jpa.Sawari.Jpa.entity.SawariOwner;

import java.util.Optional;

@Repository
public interface SawariownerReporsitory extends JpaRepository<SawariOwner,Long> {

    @Query("select t from SawariOwner t where t.id= :id")
    Optional<SawariOwner> findBySawariId(Long id);
}
