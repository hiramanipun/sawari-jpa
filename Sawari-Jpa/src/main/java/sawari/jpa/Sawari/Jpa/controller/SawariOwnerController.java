package sawari.jpa.Sawari.Jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sawari.jpa.Sawari.Jpa.dto.SawariOwnerDTO;
import sawari.jpa.Sawari.Jpa.entity.SawariOwner;
import sawari.jpa.Sawari.Jpa.repository.SawariownerReporsitory;
import sawari.jpa.Sawari.Jpa.service.SawariOwnerService;
import sawari.jpa.Sawari.Jpa.utility.FileUploadUtility;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/owners")
public class SawariOwnerController {
    @Autowired
    private SawariOwnerService sawariOwnerService;
    private final SawariownerReporsitory sawariownerReporsitory;

    public SawariOwnerController(SawariownerReporsitory sawariownerReporsitory) {
        this.sawariownerReporsitory = sawariownerReporsitory;
    }

    @PostMapping
    public ResponseEntity<SawariOwner> createOwner(@RequestBody SawariOwner sawariOwner,MultipartFile multipartFile) throws IOException {
       //String fileName = multipartFile.getOriginalFilename();
        //sawariOwner.setPhoto(FileUploadUtility.fileUpload(fileName,multipartFile));
        SawariOwner savedOwner = sawariOwnerService.create(sawariOwner);
        return new ResponseEntity<>(savedOwner, HttpStatus.CREATED);
    }
    @GetMapping("{id}")
    public ResponseEntity<SawariOwnerDTO> getById(@PathVariable long id){
        SawariOwnerDTO sawariOwnerDTO = sawariOwnerService.getByid(id);
        return new ResponseEntity<>(sawariOwnerDTO,HttpStatus.OK);
    }
    @GetMapping()
    public ResponseEntity<List<SawariOwnerDTO>> getAll(){
        List<SawariOwnerDTO> owners = sawariOwnerService.getAll();
        return new ResponseEntity<>(owners,HttpStatus.OK);
    }
    @PutMapping("{id}")
    public ResponseEntity<SawariOwnerDTO> update(@PathVariable("id") long id,@RequestBody SawariOwner sawariOwner){
        sawariOwner.setId(id);
        SawariOwnerDTO updatedOwner = sawariOwnerService.update(sawariOwner);
        return new ResponseEntity<>(updatedOwner,HttpStatus.OK);
    }
    @DeleteMapping("{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id){
        sawariOwnerService.delete(id);
        return new ResponseEntity<>("Owner deleted successfully",HttpStatus.OK);
    }
}
