package sawari.jpa.Sawari.Jpa.service.serviceImple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sawari.jpa.Sawari.Jpa.dto.*;
import sawari.jpa.Sawari.Jpa.entity.SawariOwner;
import sawari.jpa.Sawari.Jpa.entity.SawariVehicles;
import sawari.jpa.Sawari.Jpa.entity.VehicleModels;
import sawari.jpa.Sawari.Jpa.repository.VehicelDetailsRepository;
import sawari.jpa.Sawari.Jpa.service.SawariVehicleService;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SawariVehiclesImplem implements SawariVehicleService {
    @Autowired
    private VehicelDetailsRepository vehicelDetailsRepository;
    @Override
    public SawariVehiclesDTO create(SawariVehicles sawariVehicles) {
        LocalDateTime dateTime = LocalDateTime.now();
        sawariVehicles.setCreatedDate(dateTime.toString());
        sawariVehicles.setStatusChar(SawariVehicles.StatusChar.available.toString());
        sawariVehicles.setStatusNum(1);
        sawariVehicles.setFkUserId(1);
        SawariVehicles sawariVehicleSave = vehicelDetailsRepository.save(sawariVehicles);
        return this.getById(sawariVehicleSave.getId().longValue());
    }

    @Override
    public SawariVehiclesDTO update(SawariVehicles sawariVehicles) {
        return null;
    }

    @Override
    public SawariVehiclesDTO getById(Long id) {
      Optional<SawariVehicles> sawariVehicles = vehicelDetailsRepository.getVehicleDetails(id);
        SawariVehiclesDTO sawariVehiclesDTO = new SawariVehiclesDTO();
        if(!sawariVehicles.isEmpty()){
            SawariVehicles sawariVehicles1 = sawariVehicles.get();
            sawariVehiclesDTO = this.mapResult(sawariVehicles1);

        }
        return sawariVehiclesDTO;
    }
    public List<SawariVehiclesDTO> getAll() {
        List<SawariVehicles> sawariVehicles = vehicelDetailsRepository.getAll();
        List<SawariVehiclesDTO> result = new ArrayList<>();
        if(!sawariVehicles.isEmpty()){
            for(SawariVehicles sawariVehicles1:sawariVehicles){
                result.add(this.mapResult(sawariVehicles1));
            }
        }
        return result;
    }

    @Override
    public List<SawariVehiclesDTO> getBookedVehicles() {
        List<SawariVehicles> bookedSawariVehicles = vehicelDetailsRepository.getBookedVehicles();
        List<SawariVehiclesDTO> result = new ArrayList<>();
        if(!bookedSawariVehicles.isEmpty()){
            for(SawariVehicles sawariVehicles1:bookedSawariVehicles){
                result.add(this.mapResult(sawariVehicles1));
            }
        }
        return result;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }
    private SawariVehiclesDTO mapResult(SawariVehicles sawariVehicles){
        SawariVehiclesDTO sawariVehiclesDTO = new SawariVehiclesDTO();
        sawariVehiclesDTO.setTotal_seats(sawariVehicles.getTotalSeats());
        sawariVehiclesDTO.setColor(sawariVehicles.getColor());
        sawariVehiclesDTO.setAcNoAc(this.checkAc(sawariVehicles.getAcNoAc()));
        sawariVehiclesDTO.setGadiNo(sawariVehicles.getGadiNo());
        sawariVehiclesDTO.setGadikoWiwaran(sawariVehicles.getGadikoWiwaran());
        sawariVehiclesDTO.setImage(sawariVehicles.getImage());
        sawariVehiclesDTO.setCreatedDate(sawariVehicles.getCreatedDate());
        // for type
        VehicleTypeDTO vehicleTypeDTO = new VehicleTypeDTO();
        vehicleTypeDTO.setType(sawariVehicles.getVehicleType().getType());
        sawariVehiclesDTO.setVehicleTypeDTO(vehicleTypeDTO);
        // for model
        VehicleModelsDTO vehicleModelsDTO = new VehicleModelsDTO();
        vehicleModelsDTO.setModel(sawariVehicles.getVehicleModels().getModel());
        sawariVehiclesDTO.setVehicleModelsDTO(vehicleModelsDTO);
        // for location
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setLocation(sawariVehicles.getVehicleLocation().getLocation());
        sawariVehiclesDTO.setLocationDTO(locationDTO);
    return sawariVehiclesDTO;
    }
    private String checkAc(Long acValue){
        if(acValue==1){
            return "Available";
        }else{
            return "Not available";
        }
    }
}
