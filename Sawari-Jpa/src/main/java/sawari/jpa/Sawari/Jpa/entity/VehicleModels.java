package sawari.jpa.Sawari.Jpa.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sawari_vehicle_model")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class VehicleModels {
    @Id
    private Long id;
    @Column(name = "model",nullable = false)
    private String model;
    @Column(name = "created_date",nullable = false)
    private String createdDate;
    @Column(name = "status",nullable = false)
    private String status;
    @Column(name = "fk_user_id",nullable = false)
    private Long fkUserId;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_vehicle_type",referencedColumnName = "id")
    private VehicleType vehicleType;
    @OneToMany(mappedBy = "vehicleModels",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<SawariVehicles> sawariVehicles;
}
